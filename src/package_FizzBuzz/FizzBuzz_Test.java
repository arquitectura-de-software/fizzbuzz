package package_FizzBuzz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FizzBuzz_Test {

	@Test
	void Should_return_Fizz_with_number_3() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals("Fizz", fizzbuzz.generate_number(3));
	}

	@Test
	void Should_return_Buzz_with_number_5() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals("Buzz", fizzbuzz.generate_number(5));
	}

	@Test
	void Should_return_FizzBuzz_with_number_15() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals("FizzBuzz", fizzbuzz.generate_number(15));		
	}

	@Test
	void Should_return_1_with_number_1() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals("1", fizzbuzz.generate_number(1));		
	}
	
	@Test
	void Should_return_Fizz_with_number_27() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals("Fizz", fizzbuzz.generate_number(27));		
	}

	@Test
	void Should_return_98_with_number_98() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals("98", fizzbuzz.generate_number(98));		
	}

	@Test
	void Should_return_100_cases_done_when_set_100_numbers() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals(100, fizzbuzz.generate_many_numbers(100));		
	}
	
	@Test
	void Should_return_40_cases_done_when_set_40_numbers() {
		FizzBuzz_Program fizzbuzz = new FizzBuzz_Program();
		assertEquals(40, fizzbuzz.generate_many_numbers(40));		
	}
}
