package package_FizzBuzz;

import java.util.Scanner;

public class FizzBuzz_Program {
	
	public void initializate() {
		
		//Declarar variables 
		int range;
		Scanner in = new Scanner(System.in);
	
		System.out.println("Rango de Numeros:: ");
		//Leer de Consola
		range = in.nextInt();
		in.close();  
		generate_many_numbers(range);
	}
	
	
	public int generate_many_numbers(int range) {
		int control = 0;
		for (int number = 1; number <= range; number++) {
			System.out.println(generate_number(number));
			control = number;
		}
		return control;
	}
	
	
	public String generate_number(int number) {
		if (Module(number,3) && Module(number,5)) {
			return "FizzBuzz";
		}
		else {
			if (Module(number,3)) {
				return "Fizz";
			}
			else {
				if (Module(number,5)) {
					return "Buzz";
				}
				else {
					return number+"";
				}
			}
		}
		
		
	}
	
	private boolean Module(int number,int number_module) {
		if (number % number_module == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
}
